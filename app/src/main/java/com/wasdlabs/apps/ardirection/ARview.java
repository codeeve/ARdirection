package com.wasdlabs.apps.ardirection;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hoan.dsensor_master.DProcessedSensor;
import com.hoan.dsensor_master.DSensorEvent;
import com.hoan.dsensor_master.DSensorManager;
import com.hoan.dsensor_master.interfaces.DProcessedEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* implement sensor listener */
public class ARview extends AppCompatActivity implements LocationListener {
    private static String TAG = "AR View";
    private Camera mCamera = null;
    private CameraView mCameraView = null;

    Float azimut;  // sudut azimut untuk arah compass
    float rotation; //sudut dari arah utara
    double camAngle;
    int screenwidth, containerWidth;

    int childCount = 0;

    //var untuk gps
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;
    private double gpsLat, gpsLng;
    protected boolean gps_enabled, network_enabled;

    String[] nama;
    double[] lat, lng;
    double[] locationBearing;

    List<ImageView> imageViews;
    ImageView[] imageViewArray;


    FirebaseDatabase database;
    DatabaseReference myRef;

    RelativeLayout markerContainer;

    private boolean firstSetup = true;

    //variable buat buka direction
    double clickedLat = 0;
    double clickedLong = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arview);

        markerContainer = (RelativeLayout) findViewById(R.id.markercontainer);
/**
 * ambil data lokasi dari firebase
 */

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        Query myQuery = myRef.child("location");

        //set singleton data buat nampung datanya
        Data.getInstance();
        Data.getInstance().setLocationDataList();
        imageViews = new ArrayList<ImageView>();
        myQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.e(TAG, "onChildAdded:" + dataSnapshot.getKey());
                childCount = (int) dataSnapshot.getChildrenCount();

                LocationData locData = dataSnapshot.getValue(LocationData.class);

                Log.e(TAG, "data child berhasil ditarik");
                Log.e(TAG, "data child => nama:" +locData.nama+ " | lat:"+locData.lat+" | lng:"+locData.lng+" | ket:"+locData.ket);

                Data.getInstance().locationDataList.add(locData);
                updateBearing();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        /** ambil extras
         *
         */
        /*Bundle extras = getIntent().getExtras();
        nama = extras.getStringArray("nama");
        lat = extras.getDoubleArray("lat");
        lng = extras.getDoubleArray("lng");
        locationBearing = new double[nama.length];
*/
        //Toast.makeText(ARview.this, "nama 0: " + nama[0], Toast.LENGTH_SHORT).show();

        /**
         * bikin GPS
         */
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 50, this);

        Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (lastKnownLocationGPS != null)
        {
            gpsLat = lastKnownLocationGPS.getLatitude();
            gpsLng = lastKnownLocationGPS.getLongitude();
        } else {
            gpsLat = -5.210290;
            gpsLng = 119.473930;
        }


        Log.e("gps", "lat "+gpsLat+" long"+gpsLng);

        /*gpsLat = lastKnownLocationGPS.getLatitude();
        gpsLng = lastKnownLocationGPS.getLongitude();
        updateBearing();

        //set text
        TextView mPosition = (TextView) findViewById(R.id.cameraparam);
        mPosition.setText("lat:"+gpsLat+" long:"+gpsLng);
        */


        /**
         * buka kamera
         */
        try{
            mCamera = Camera.open();//you can use open(int) to use different cameras
        } catch (Exception e){
            Log.d("ERROR", "Failed to get camera: " + e.getMessage());
        }

        if(mCamera != null) {
            mCameraView = new CameraView(this, mCamera);//create a SurfaceView to show camera data
            FrameLayout camera_view = (FrameLayout)findViewById(R.id.camera_view);
            camera_view.addView(mCameraView);//add the SurfaceView to the layout

            //get camera parameter
            camAngle= Math.toRadians(mCamera.getParameters().getVerticalViewAngle());
            Log.i(getPackageName(), "cam angle: " + camAngle + " rad");

            //get screen width
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenwidth = metrics.widthPixels;
            Log.e(getPackageName(), "screen width: " + screenwidth + " px");

            containerWidth = (int) ((2/camAngle)*screenwidth);
            Log.e(getPackageName(), "container width: " + containerWidth + " px");

            //set container width
            //RelativeLayout markerContainer = (RelativeLayout) findViewById(R.id.markercontainer);
            markerContainer.getLayoutParams().width= containerWidth;
        }


    }


    @Override
    protected void onResume() {
        super.onResume();

        DSensorManager.startDProcessedSensor(this, DProcessedSensor.TYPE_3D_COMPASS, new DProcessedEventListener() {
            @Override
            public void onProcessedValueChanged(DSensorEvent dSensorEvent) {

                /** azimuth: dSensorEvent.values[0]
                azimuth di return dalam bentuk phi.
                // north = 0
                // east = + 3.14/2
                // west = - 3.14/2
                // south = 3.14 atau -3.14
                // nilai minus = perangkat berputar ke kiri
                // nilai plus = perangkat berputar ke kanan
                 */
                azimut = dSensorEvent.values[0];
                // hitung rotasi dalam bentuk sudut
                rotation = -azimut * 360 / (2 * 3.14159f);

                setLocationMarker();


            }
        });
    }

    @Override
    protected void onPause() {
        DSensorManager.stopDSensor();
        super.onPause();

        //  TextView lol = (TextView) findViewById(R.id.lol);
        //lol.setText("azimuth: " + processedSensorEvent.zAxisDirection);
    }

    private void setRotationText(String text){
        //rotation = -azimut * 360 / (2 * 3.14159f); //hitung sudut dari arah utara
        TextView tvRotation = (TextView) findViewById(R.id.rotation);
        tvRotation.setText(text);
    }

    private void setCameraParamText(String text){
        TextView camparam = (TextView) findViewById(R.id.cameraparam);
        camparam.setText(text);
    }

    private void setLocationMarker(){
        //TODO set marker lokasinya di sini

        //set lokasi cuma kalau semua data child sudah terload
        int dataSize = Data.getInstance().locationDataList.size();
        //Log.e(TAG, "set location marker");


        if(dataSize!=0) {
            //setRotationText("azi: " + azimut + " \nrot: " + rotation);

            //set image view array
            if(firstSetup) {
                imageViewArray = new ImageView[dataSize];
            }

            //kosongkan view
            //markerContainer.removeAllViews();
            //Log.e(TAG, "all marker removed");


            //setRotationText("rot: " + rotation);

            //set planar position di sini
            //set marker position
            //dummy data. marker di posisi 90 derajat
            //double markerBearing1 = Data.getInstance().locationDataList.get(0).bearing;
            //Log.e(TAG, "marker 1 bearing:" + markerBearing1);

            //rumus posisi saat dipetakan di AR
            // x = (markerbearing * containerWidth) / 360;
            //int posisidicontainer = (int) (((markerBearing1 * containerWidth) / 360) + (screenwidth / 2));
            int pixelPerDegree = (int) containerWidth / 360;

            //getmarker
            for(int i = 0; i<dataSize; i++){
                //tarik bearing untuk marker ini

                //Log.e(TAG, "marker "+i+" bearing:" + markerBearing);

                if(firstSetup) {
                    //kalau pertama kali masukkan image view

                    imageViewArray[i] = new ImageView(this);
                    //setting image resource
                    imageViewArray[i].setImageResource(R.drawable.marker);


                    //setting image position
                    imageViewArray[i].setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT));

                    //masukkan imageview kedalam container
                    markerContainer.addView(imageViewArray[i]);
                    Log.e(TAG, "marker "+i+" nama lokasi:" + Data.getInstance().locationDataList.get(i).nama);

                    //set onclick listener
                    final int finalI = i;
                    imageViewArray[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.e(TAG,"clicked "+Data.getInstance().locationDataList.get(finalI).nama );
                            //tampilkan keterangan
                            setRotationText(Data.getInstance().locationDataList.get(finalI).ket);
                            //set button buat buka peta
                            clickedLat = Data.getInstance().locationDataList.get(finalI).lat;
                            clickedLong = Data.getInstance().locationDataList.get(finalI).lng;

                        }
                    });
                }


                double markerBearing = Data.getInstance().locationDataList.get(i).bearing;

                int posisidicontainer = (int) (((markerBearing * containerWidth) / 360) + (screenwidth / 2));

                imageViewArray[i].setX((posisidicontainer + (rotation * pixelPerDegree))%containerWidth);

                Log.e(TAG, "posisi kontainer:"+posisidicontainer+"ppd:"+pixelPerDegree+" marker " + i + " position:" + imageViewArray[i].getX()+" rotation:"+rotation);

                if((dataSize == i+1) && (firstSetup)){
                    Log.e(TAG, "first setup done! added "+i+" data");
                    firstSetup=false;
                }
                //Log.e("child count", "child count:"+markerContainer.getChildCount());

            }


            //final ImageView marker1 = (ImageView) findViewById(R.id.marker1);
            //marker1.setX(posisidicontainer + (rotation * pixelPerDegree));
        }

        //show child count
        //Log.e("child count", "child: "+markerContainer.getChildCount());

    }

    @Override
    public void onLocationChanged(Location location) {
        gpsLat = location.getLatitude();
        gpsLng = location.getLongitude();
        Log.d("location update", "lat:"+gpsLat+" long:"+gpsLng);

        TextView mPosition = (TextView) findViewById(R.id.cameraparam);
        mPosition.setText("lat:" + gpsLat + " long:" + gpsLng);

        updateBearing();

        //update planar mapping di sini
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void updateBearing(){
        //update bearing masing-masing titik
        int arraySize = Data.getInstance().locationDataList.size();
        for (int i=0; i<arraySize; i++ ){
            double lat = Data.getInstance().locationDataList.get(i).lat;
            double lng = Data.getInstance().locationDataList.get(i).lng;
            Data.getInstance().locationDataList.get(i).bearing = calculateBearing(gpsLat, gpsLng, lat, lng);
            Log.e(TAG, "bearing data updated. data ke-"+i+" => "+Data.getInstance().locationDataList.get(i).bearing);
        }
    }

    protected static double calculateBearing(double lat1, double lon1, double lat2, double lon2){

        double longDiff= lon2-lon1;
        double y = Math.sin(longDiff)*Math.cos(lat2);
        double x = Math.cos(lat1)*Math.sin(lat2)-Math.sin(lat1)*Math.cos(lat2)*Math.cos(longDiff);

        return ( Math.toDegrees(Math.atan2(y, x)) + 360 ) % 360;
    }

    //fungsi on click buat container informasi
    public void gotoDirection(View view){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr="+clickedLat+","+clickedLong));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

        startActivity(intent);
    }


}
