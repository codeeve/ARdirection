package com.wasdlabs.apps.ardirection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ratten on 10-Aug-16.
 */
public class Data {

    private static Data mInstance = null;

    public List<LocationData> locationDataList;

    private Data(){
    }

    public static Data getInstance(){
        if(mInstance == null)
        {
            mInstance = new Data();

        }
        return mInstance;
    }


    public List<LocationData> getLocationDataList(){
        return this.locationDataList;
    }

    public void setLocationDataList(){
        this.locationDataList = new ArrayList<LocationData>();
    }

    public void addSingle(LocationData locationData){
        this.locationDataList.add(locationData);
    }

}
