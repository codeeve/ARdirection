package com.wasdlabs.apps.ardirection;

/**
 * Created by Ratten on 10-Aug-16.
 */
public class LocationData {
    public String nama;
    public String ket;
    public double lat;
    public double lng;
    public double bearing;

    public LocationData() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public LocationData(String nama,String ket, double lat, double lng) {
        this.nama = nama;
        this.ket = ket;
        this.lat = lat;
        this.lng = lng;
        this.bearing = 0;
    }

}
