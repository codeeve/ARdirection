package com.wasdlabs.apps.ardirection;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.hoan.dsensor_master.DProcessedSensor;
import com.hoan.dsensor_master.DProcessedSensorEvent;
import com.hoan.dsensor_master.DSensor;
import com.hoan.dsensor_master.DSensorEvent;
import com.hoan.dsensor_master.DSensorManager;
import com.hoan.dsensor_master.interfaces.DProcessedEventListener;
import com.hoan.dsensor_master.interfaces.DSensorEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        /*
        delay 1000ms sebelum masuk ke tampilan ARview
        */

        /*
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                final Intent mainIntent = new Intent(SplashScreen.this, ARview.class);
                startActivity(mainIntent);
                finish();
            }
        }, 1000);
        */

    }


    public void gotoARview(View view){
        final Intent mainIntent = new Intent(SplashScreen.this, ARview.class);
        startActivity(mainIntent);
        //finish();
    }

    public void exit(View view){
        //this.finishAffinity();
        final Intent mainIntent = new Intent(SplashScreen.this, ActivityHelp.class);
        startActivity(mainIntent);
    }

    public void about(View view){
        final Intent mainIntent = new Intent(SplashScreen.this, ActivityAbout.class);
        startActivity(mainIntent);
    }


}
